package com.example.contador;

import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener {
    protected static final String ESTADO_PAUSA = "tiempo de la pausa" ;
    protected static final String ESTADO_CAFES = "número de cafés" ;
    protected static final String ESTADO_SWITCH= "contador Up/Down";
    protected static final String ESTADO_CUENTA= "contando?";
    protected static final String ESTADO_TIEMPO= "tiempo actual del contador";

    private static final int PAUSA = 3;

    Button menos, mas, comenzar;
    TextView txtCafes, txtTiempo;
    MyCountDownTimer contadorDescendente;
    Contador contador;
    Switch opcion;
    long tiempoActual = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        setContentView(R.layout.activity_main);

        menos = (Button) findViewById(R.id.button1);
        mas = (Button) findViewById(R.id.button2);
        comenzar = (Button) findViewById(R.id.button3);
        menos.setOnClickListener(this);
        mas.setOnClickListener(this);
        comenzar.setOnClickListener(this);

        txtCafes = (TextView) findViewById(R.id.textView2);
        txtTiempo = (TextView) findViewById(R.id.textView3);
        opcion = (Switch) findViewById(R.id.switch1);
        opcion.setChecked(true);

        if  (savedInstanceState == null) {
            contador = new Contador(0, PAUSA);
            txtTiempo.setText(PAUSA + ":00");
        }
    }

    @Override
    public void onClick(View v) {
        if (v == menos)
            txtTiempo.setText(contador.disminuirTiempo() + ":00");

        if (v == mas)
            txtTiempo.setText(contador.aumentarTiempo() + ":00");

        if (v == comenzar) {
            contadorDescendente = new MyCountDownTimer(contador.getTiempo() * 60 * 1000, 1000);
            contadorDescendente.start();
            menos.setEnabled(false);
            mas.setEnabled(false);
            comenzar.setEnabled(false);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Salvar las variables al cambiar la orientación
        savedInstanceState.putInt(ESTADO_PAUSA, contador.getTiempo());
        savedInstanceState.putInt(ESTADO_CAFES, contador.getCafes());
        savedInstanceState.putBoolean(ESTADO_SWITCH, opcion.isChecked());
        Boolean contando = !comenzar.isEnabled();
        savedInstanceState.putBoolean(ESTADO_CUENTA, contando);
        if (contando)
            savedInstanceState.putLong(ESTADO_TIEMPO, tiempoActual);

        // Siempre se debe llamar al constructor de la clase superior
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        int pausa, cafes;
        boolean down, contando;
        long tiempo;
        // Siempre se debe llamar al constructor de la clase superior
        super.onRestoreInstanceState(savedInstanceState);

        // Restaurar las variables al cambiar la orientación
        pausa = savedInstanceState.getInt(ESTADO_PAUSA);
        cafes = savedInstanceState.getInt(ESTADO_CAFES);
        down = savedInstanceState.getBoolean(ESTADO_SWITCH);
        contando = savedInstanceState.getBoolean(ESTADO_CUENTA);
        tiempo = savedInstanceState.getLong(ESTADO_TIEMPO);
        contador = new Contador(cafes, pausa);
        txtCafes.setText(String.valueOf(cafes));
        opcion.setChecked(down);
        if (contando) {
                contadorDescendente = new MyCountDownTimer(tiempo, 1000, pausa * 60 * 1000 - tiempo);
                contadorDescendente.start();
                menos.setEnabled(false);
                mas.setEnabled(false);
                comenzar.setEnabled(false);
            }
        else
                txtTiempo.setText(contador.getTiempo() + ":00");
    }

    public class MyCountDownTimer extends CountDownTimer {
        long total, intervalo, minutos, segundos;
        boolean sentido;

        public MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
            total = startTime;
            sentido = opcion.isChecked();
        }

        public MyCountDownTimer(long startTime, long interval, long anterior) {
            super(startTime, interval);
            total = startTime + anterior;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            tiempoActual = millisUntilFinished;
            sentido = opcion.isChecked();
            if (sentido)
                intervalo = millisUntilFinished;
            else
                intervalo = total - millisUntilFinished;

            minutos = (intervalo / 1000) / 60;
            segundos = (intervalo / 1000) % 60;
            txtTiempo.setText(minutos + ":" + segundos);
        }

        @Override
        public void onFinish() {
            txtTiempo.setText("Pausa terminada!!");
            txtCafes.setText(contador.aumentarCafes());
            menos.setEnabled(true);
            mas.setEnabled(true);
            comenzar.setEnabled(true);
            //opcion.setEnabled(true);
        }
    }
}