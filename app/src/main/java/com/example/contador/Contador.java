package com.example.contador;

/**
 * Created by paco on 1/10/15.
 */
public class Contador {
    private int cafes;
    private int tiempo;

    public Contador () {
        this.cafes = 0;
        this.tiempo = 5 * 60 * 1000;
    }
    public Contador (int c, int t) {
        this.cafes = c;
        this.tiempo = t;
    }

    public String aumentarTiempo(){
        this.tiempo += 1;
        return String.valueOf(this.tiempo);
    }
    public String disminuirTiempo(){
        tiempo -=  1;
        if (tiempo < 1)
            tiempo = 1;
        return String.valueOf(tiempo);
    }
    public String aumentarCafes(){
        this.cafes +=1;
        return String.valueOf(this.cafes);
    }

    public int getTiempo() {
        return tiempo;
    }

    public int getCafes() {
        return cafes;
    }
}
